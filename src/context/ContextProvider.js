import {createContext, useState} from 'react';

export const Context = createContext(null);

const ContextProvider = ({children}) => {
  const [userData, setUserData] = useState(null);

  const value = {
    userData,
    setUserData,
  };

  return <Context.Provider value={value}>{children}</Context.Provider>;
};

export default ContextProvider;
