const strings = {
  // App Strings
  dark: 'dark',

  // AppNavigation Strings
  dashboard: 'Dashboard',

  // AuthNavigation Strings
  login: 'Login',
  splash: 'Splash',

  // Button Strings
  button: 'Button',
  light: 'light',
  solid: 'solid',

  // Environment Strings
  development: 'development',
  staging: 'staging',
  production: 'production',

  // Fetch Strings
  error: 'Error',
  success: 'Success',

  // FloatingTextInput Strings
  backspace: 'Backspace',
  star: '*',
  title: 'Title',

  // Login Strings
  exitText: 'Press back again to exit RN GraphQL app!',
  emailAddress: 'Email Address',
  password: 'Password',

  // NoResponse Strings
  whoops: 'Whoops!',
  notFindAnything: "We couldn't find anything!",

  // Security Strings
  background: 'background',
  inactive: 'inactive',

  // Error Strings
  emailAddressError: 'Email address is required!',
  emailAddressInvalidError: 'Email address is invalid!',
  passwordError: 'Password is required!',
  passwordCountError: 'Password must have atleast 8 characters!',
  passwordInvalidError: 'Password must be alphanumeric!',
};

export default strings;
