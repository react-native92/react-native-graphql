import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Labels from '../utils/Strings';
import Dashboard from '../screens/appScreens/Dashboard';

const AppNavigation = () => {
  // AppNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group screenOptions={() => ({headerShown: false})}>
      <Stack.Screen name={Labels.dashboard} component={Dashboard} />
    </Stack.Group>
  );
};

export default AppNavigation;
