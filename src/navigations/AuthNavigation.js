import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Labels from '../utils/Strings';
import Login from '../screens/authScreens/Login';
import Splash from '../screens/authScreens/Splash';

const AuthNavigation = () => {
  // AuthNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group screenOptions={() => ({headerShown: false})}>
      <Stack.Screen name={Labels.login} component={Login} />
      <Stack.Screen name={Labels.splash} component={Splash} />
    </Stack.Group>
  );
};

export default AuthNavigation;
