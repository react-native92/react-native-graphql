import React, {useCallback, useContext} from 'react';
import {Image, View} from 'react-native';
import {Context} from '../../context/ContextProvider';
import {useFocusEffect} from '@react-navigation/core';
import {useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ENV from '../../../env';
import Labels from '../../utils/Strings';
import Queries from '../../api/Queries';
import RequestService from '../../api/Service';
import Styles from '../../styles/authStyles/Splash';
import * as HelperStyles from '../../utils/HelperStyles';

const Splash = props => {
  // Context Variables
  const {setUserData} = useContext(Context);

  // Splash Variables

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      init();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const init = async () => {
    const userId = await AsyncStorage.getItem('userId');

    if (Boolean(userId)) {
      const requestData = {query: Queries.fetchUser(userId)};

      ENV.currentEnvironment == Labels.development &&
        console.log('SPLASH REQUEST DATA::: ', requestData);

      RequestService.post(requestData, response => {
        const userData = response?.data?.user ?? null;

        Boolean(userData) && [
          AsyncStorage.setItem('userId', userData.id),

          setUserData(userData),

          props.navigation.navigate(Labels.dashboard),
        ];
      });
    } else {
      setTimeout(() => {
        props.navigation.navigate(Labels.login);
      }, 2500);
    }
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={Styles.screenContainer}>
        <View style={HelperStyles.screenSubContainer}>
          <Image
            resizeMode={'contain'}
            source={Assets.logo}
            style={HelperStyles.imageView(120, 120)}
          />
        </View>
      </View>
    </View>
  );
};

export default Splash;
