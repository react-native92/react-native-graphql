import React, {useCallback, useContext, useState} from 'react';
import {BackHandler, Image, ScrollView, Text, View} from 'react-native';
import {Context} from '../../context/ContextProvider';
import {showMessage} from 'react-native-flash-message';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Button from '../../components/appComponents/Button';
import Card from '../../containers/Card';
import ENV from '../../../env';
import FloatingTextInput from '../../components/appComponents/FloatingTextInput';
import Labels from '../../utils/Strings';
import Queries from '../../api/Queries';
import RequestService from '../../api/Service';
import Styles from '../../styles/authStyles/Login';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const Login = props => {
  // Context Variables
  const {setUserData} = useContext(Context);

  // Login Variables
  const [exitCount, setExitCount] = useState(0);
  const [emailAddress, setEmailAddress] = useState(null);
  const [password, setPassword] = useState(null);

  // Error Variables
  const [emailAddressError, setEmailAddressError] = useState(false);
  const [emailAddressInvalidError, setEmailAddressInvalidError] =
    useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordInvalidError, setPasswordInvalidError] = useState(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () => {
        isFocus = false;

        BackHandler.removeEventListener('hardwareBackPress', backAction);
      };
    }, [exitCount]),
  );

  const backAction = () => {
    setTimeout(() => {
      setExitCount(0);
    }, 2000);

    switch (exitCount) {
      case 0:
        setExitCount(exitCount + 1);

        showMessage({
          icon: 'auto',
          message: Labels.exitText,
          position: 'bottom',
          type: 'default',
        });
        break;

      case 1:
        BackHandler.exitApp();
        break;

      default:
        break;
    }

    return true;
  };

  const handleEmailAddress = txt => {
    const isValidEmailAddress = Helpers.validateEmail(txt);

    emailAddressError && setEmailAddressError(false);

    setEmailAddress(Boolean(txt) ? txt : null);

    setEmailAddressInvalidError(isValidEmailAddress);
  };

  const handlePassword = txt => {
    const isValidPassword = Helpers.validatePassword(txt);

    passwordError && setPasswordError(false);

    setPassword(Boolean(txt) ? txt : null);

    setPasswordInvalidError(isValidPassword);
  };

  const handleLogin = () => {
    if (checkLogin()) {
      const requestData = handleRequestData();

      ENV.currentEnvironment == Labels.development &&
        console.log('LOGIN REQUEST DATA::: ', requestData);

      setTimeout(() => {
        handleAPIRequest(requestData);
      }, 2500);
    } else {
      handleErrorValidation();
    }
  };

  const checkLogin = () => {
    if (
      Boolean(emailAddress) &&
      !emailAddressInvalidError &&
      Boolean(password) &&
      !passwordInvalidError
    ) {
      return true;
    } else {
      return false;
    }
  };

  const handleRequestData = () => {
    let requestData = {};

    requestData = {query: Queries.login(emailAddress, password)};

    return requestData;
  };

  const handleAPIRequest = requestData => {
    RequestService.post(requestData, response => {
      const userData = response?.data?.user ?? null;

      Boolean(userData) && [
        AsyncStorage.setItem('userId', userData.id),

        setUserData(userData),

        handleReset(),

        props.navigation.navigate(Labels.dashboard),
      ];
    });
  };

  const handleErrorValidation = () => {
    setEmailAddressError(Boolean(emailAddress) ? false : true);
    setPasswordError(Boolean(password) ? false : true);
  };

  const handleReset = () => {
    // Login Variables
    setEmailAddress(null);
    setPassword(null);

    // Error Variables
    setEmailAddressError(false);
    setEmailAddressInvalidError(false);
    setPasswordError(false);
    setPasswordInvalidError(false);
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}>
        <View style={Styles.screenContainer}>
          <Image
            resizeMode={'contain'}
            source={Assets.logo}
            style={HelperStyles.imageView(96, 96)}
          />

          <View style={HelperStyles.screenSubContainer}>
            <Card containerStyle={HelperStyles.margin(0, 8)}>
              <FloatingTextInput
                autoCapitalize={'none'}
                keyboardType={'email-address'}
                textContentType={'emailAddress'}
                title={Labels.emailAddress}
                updateMasterState={txt => {
                  handleEmailAddress(txt);
                }}
                value={emailAddress}
              />
            </Card>

            {emailAddressError && (
              <Text
                style={[
                  HelperStyles.errorText,
                  HelperStyles.justView('marginHorizontal', 8),
                ]}>
                {Labels.emailAddressError}
              </Text>
            )}

            {Boolean(emailAddress) && emailAddressInvalidError && (
              <Text
                style={[
                  HelperStyles.errorText,
                  HelperStyles.justView('marginHorizontal', 8),
                ]}>
                {Labels.emailAddressInvalidError}
              </Text>
            )}

            <Card containerStyle={HelperStyles.margin(0, 8)}>
              <FloatingTextInput
                autoCapitalize={'none'}
                ispassword={true}
                keyboardType={'default'}
                otherTextInputProps={{maxLength: 20}}
                showPasswordIcon={true}
                textContentType={'newPassword'}
                title={Labels.password}
                updateMasterState={txt => {
                  handlePassword(txt);
                }}
                value={password}
              />
            </Card>

            {passwordError && (
              <Text
                style={[
                  HelperStyles.errorText,
                  HelperStyles.justView('marginHorizontal', 8),
                ]}>
                {Labels.passwordError}
              </Text>
            )}

            {Boolean(password) && passwordInvalidError && (
              <Text
                style={[
                  HelperStyles.errorText,
                  HelperStyles.justView('marginHorizontal', 8),
                ]}>
                {password.length < 6
                  ? Labels.passwordCountError
                  : Labels.passwordInvalidError}
              </Text>
            )}

            <Button
              containerStyle={HelperStyles.margin(0, 24)}
              label={Labels.login}
              loading={Boolean(props.loadingStatus)}
              onPress={() => {
                handleLogin();
              }}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Login;
