import React from 'react';
import {Text, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Colors from '../../utils/Colors';
import Labels from '../../utils/Strings';
import Styles from '../../styles/appStyles/Dashboard';
import * as HelperStyles from '../../utils/HelperStyles';

const Dashboard = props => {
  // Dashboard Variables

  // Theme Variables
  const Theme = useTheme().colors;

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={Styles.screenContainer}>
        <View style={HelperStyles.screenSubContainer}>
          <Text
            style={HelperStyles.textView(16, '600', Colors.primary, 'center')}>
            {Labels.dashboard}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Dashboard;
