const assets = {
  // Gifs
  indicator: require('./gifs/indicator.gif'),
  noResponse: require('./gifs/noResponse.gif'),

  // Images
  logo: require('./images/logo.png'),
  logoRounded: require('./images/logoRounded.png'),
  logoSquare: require('./images/logoSquare.png'),
};

export default assets;
