import ENV from '../../env';
import Fetch from './Fetch';
import * as Endpoints from './Endpoints';

class HandleRequestService {
  endPoint = Endpoints.graphQL;

  delete = (requestData, onResponse, showFlashMessage = true) => {
    handleRequestHeaders(
      'DELETE',
      `${this.endPoint}?query=${requestData}`,
      null,
      onResponse,
      showFlashMessage,
    );
  };

  get = (requestData, onResponse, showFlashMessage = true) => {
    handleRequestHeaders(
      'GET',
      `${this.endPoint}?query=${requestData}`,
      null,
      onResponse,
      showFlashMessage,
    );
  };

  patch = (requestData, onResponse, showFlashMessage = true) => {
    handleRequestHeaders(
      'DELETE',
      this.endPoint,
      requestData,
      onResponse,
      showFlashMessage,
    );
  };

  post = (requestData, onResponse, showFlashMessage = true) => {
    handleRequestHeaders(
      'POST',
      this.endPoint,
      requestData,
      onResponse,
      showFlashMessage,
    );
  };

  put = (requestData, onResponse, showFlashMessage = true) => {
    handleRequestHeaders(
      'PUT',
      this.endPoint,
      requestData,
      onResponse,
      showFlashMessage,
    );
  };
}

const handleRequestHeaders = async (
  methodType,
  endPoint,
  requestData,
  onResponse,
  showFlashMessage,
) => {
  let requestHeader = {
    method: methodType,
    headers: {
      Accept: '*/*',
      'Content-Type':
        requestData instanceof FormData
          ? 'multipart/form-data'
          : 'application/json',
    },
  };

  Boolean(requestData) && [
    (requestHeader = {
      ...requestHeader,
      body:
        requestData instanceof FormData
          ? requestData
          : JSON.stringify(requestData),
    }),
  ];

  Fetch.handleFetchRequest(
    `${ENV.baseURL}${endPoint}`,
    requestHeader,
    onResponse,
    showFlashMessage,
  );
};

const RequestService = new HandleRequestService();

export default RequestService;
