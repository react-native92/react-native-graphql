const Queries = {
  fetchUser: id => `query { user (id: ${id}) { id name email } }`,
  login: (email, password) =>
    `query { user (email: "${email}", password: "${password}") { id name email } }`,
};
export default Queries;
