import {showMessage} from 'react-native-flash-message';
import Labels from '../utils/Strings';
import ENV from '../../env';

class HandleFetchRequest {
  handleFetchRequest = (endPoint, headers, onResponse, showFlashMessage) => {
    ENV.currentEnvironment == Labels.development &&
      console.log('REQUEST ENDPOINT::: ', endPoint);

    fetch(endPoint, headers)
      .then(handleResponse)
      .then(resJson => {
        ENV.currentEnvironment == Labels.development &&
          console.log('RESJSON:::: ', resJson);

        handleSuccessResponse(resJson, onResponse, showFlashMessage);
      })
      .catch(errorResponse => {
        ENV.currentEnvironment == Labels.development &&
          console.log('ERROR RESPONSE:::: ', errorResponse);

        handleErrorResponse(errorResponse);
      });
  };
}

const handleResponse = async response => {
  const statusCode = response.status;

  const resJson = response.json();

  return Promise.all([statusCode, resJson]).then(res => ({
    statusCode: res[0],
    resJson: res[1],
  }));
};

const handleSuccessResponse = (
  successResponse,
  onResponse,
  showFlashMessage,
) => {
  const response = successResponse.resJson;

  if (successResponse.statusCode == 200) {
    response.message &&
      showFlashMessage &&
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Labels.success,
        type: 'success',
      });

    onResponse(response);
  } else {
    const errorMessage =
      response.hasOwnProperty('errors') &&
      Array.isArray(response.errors) &&
      response.errors[0].message;

    showMessage({
      description: errorMessage || response.message,
      icon: 'auto',
      message: Labels.error,
      type: 'danger',
    });
  }
};

const handleErrorResponse = errorResponse => {
  showMessage({
    description: errorResponse.toString().split(': ')[1],
    icon: 'auto',
    message: Labels.error,
    type: 'danger',
  });
};

const FetchRequest = new HandleFetchRequest();

export default FetchRequest;
