import React from 'react';
import {ActivityIndicator, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../utils/Colors';
import Labels from '../../utils/Strings';
import Styles from '../../styles/otherStyles/Button';
import * as HelperStyles from '../../utils/HelperStyles';

const Button = ({
  containerStyle = null,
  isImage = false,
  label = Labels.button,
  loading = false,
  mode = Labels.solid,
  onPress = () => {},
  renderImage = () => {},
  textStyle = null,
  touchable = false,
}) => {
  // Button Variables

  // Other Variables
  let customContainerStyle = {},
    customTextStyle = {};

  switch (mode) {
    case Labels.light:
      customContainerStyle = {
        ...customContainerStyle,
        backgroundColor: Colors.solitude,
        borderColor: Colors.solitude,
        borderRadius: 4,
        borderWidth: 0.5,
      };

      customTextStyle = {color: Colors.martinique, top: isImage ? 2 : 0};
      break;

    case Labels.solid:
    default:
      customContainerStyle = {
        ...customContainerStyle,
        backgroundColor: Colors.primary,
        borderColor: Colors.primary,
        borderRadius: 4,
        borderWidth: 0.5,
      };

      customTextStyle = {color: Colors.white, top: isImage ? 2 : 0};
      break;
  }

  const handleOnPress = () => {
    onPress();
  };

  const renderButton = () => {
    return (
      <View
        style={[
          HelperStyles.flex(1),
          HelperStyles.justifyContentCenteredView('center'),
        ]}>
        <View style={HelperStyles.justifyContentCenteredView('center')}>
          {loading ? (
            <ActivityIndicator size="small" color={Colors.white} />
          ) : (
            <View
              style={[
                HelperStyles.flex(1),
                HelperStyles.flexDirection('row'),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              {isImage && renderImage()}
              <Text
                style={[
                  HelperStyles.textView(
                    16,
                    '600',
                    Colors.white,
                    'center',
                    'capitalize',
                  ),
                  customTextStyle,
                  textStyle,
                ]}>
                {label}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  };

  return (
    <TouchableOpacity
      disabled={touchable || loading}
      style={[
        Styles.buttonContainer,
        customContainerStyle,
        containerStyle,
        loading && HelperStyles.justView('backgroundColor', Colors.primary),
      ]}
      onPress={() => {
        handleOnPress();
      }}>
      {renderButton()}
    </TouchableOpacity>
  );
};

export default Button;
