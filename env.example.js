const environmentTypes = {
  production: {
    baseURL: 'http://10.0.2.2:5000/',
  },
  staging: {
    baseURL: 'http://10.0.2.2:5000/',
  },
  development: {
    baseURL: 'http://10.0.2.2:5000/',
  },
};

const setEnvironment = () => {
  // Insert Current Platform (e.g. development, production, etc)
  const currentEnvironment = 'development';

  return {...environmentTypes[currentEnvironment], currentEnvironment};
};

const ENV = setEnvironment();

export default ENV;
